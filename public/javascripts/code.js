/* global $ */

function groupSessions(sessions){

    var grouped = {};
    for(var i=0; i<sessions.length; i++){
        var ses = sessions[i];
        if(typeof grouped[ses.email] === 'undefined'){
            grouped[ses.email] = [];
        }
        grouped[ses.email].push(ses);
    }
    var cleanGroup = [];
    for(var group in grouped){
        cleanGroup.push(grouped[group]);
    }
    return cleanGroup;
}

function populateFEList(sessions){
    
    var group = groupSessions(sessions);
    if(group.length ===0){
        document.getElementById('loading_result').classList.add('hide');
        document.getElementById('empty_result').classList.remove('hide');
        return;
    }
    var template;
    var target = document.querySelector('#result_table tbody');
    
    for(var email, i=0; i<group.length; i++){
        email = group[i][0].email;
        template = document.querySelector('template').content.querySelector('tr.list').cloneNode(true);
        template.querySelector('.email').innerHTML = email;
        template.querySelector('.lastVisit').innerHTML = group[i][group[i].length-1].pageTitle;
        placeTemplateEvent(template, email);
        target.appendChild(template);    
    }
    document.getElementById('loading_result').classList.add('hide');
    document.getElementById('display_result').classList.remove('hide');
}

function placeTemplateEvent(el, email){
    el.addEventListener('click', function(){
       window.location.href= '/session/' + email;
    });
}

function populateFESession(session){
    if(!session || session.length === 0){
        return;
    }
    var email = session[0].email;
    document.querySelector('.session_email').innerHTML = email;
    var template;
    var target = document.querySelector('div.content');
    for(var i=0; i<session.length;i++){
        for(var sess, j=0; j<session[i].userSession.length; j++){
            template = document.querySelector('template').content.querySelector('div.item').cloneNode(true);
            sess = session[i].userSession[j];
            template.querySelector('.pageTitle').innerHTML = sess.pageTitle;
            template.querySelector('.timestamp').innerHTML = sess.timestamp;
            target.appendChild(template);
        }
    }
}
