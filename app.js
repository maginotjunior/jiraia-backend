/*eslint no-new-require: "off"*/
var express = require('express');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);

var redis_url, rPort, rHost;
if (process.env.REDIS_URL) {
	redis_url = process.env.REDIS_URL;
} else {
	rPort = 6379;
	rHost = '127.0.0.1';
	redis_url = "redis://" + rHost + ":" + rPort;
}

var redisStore = new RedisStore({
	url: redis_url
});
	
const crypto = require('crypto');
const passport = require('passport');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var api = require('./routes/api');

const LocalStrategy = require('passport-local').Strategy;
const serverConfig = require('./serverConfig');

var connPath;

if(process.env.MONGODB_URI) {
    connPath =process.env.MONGODB_URI;
    console.log('MONGODB_URI It is set! ENV: ' + connPath); 
}
else { 
    console.log('MONGODB_URI No set!'); 
    connPath = serverConfig.dbProtocol + serverConfig.dbServerAddress + serverConfig.dbPath;	
}

console.log('Conectando no Banco de Dados: ' + connPath);

var mongoose = new require("mongoose");
mongoose.connect(connPath);

/* usuários do sistema */
require('./models/Users');
var Users = mongoose.model("Users");

var db = mongoose.connection; 
db.once('open', function () {
	console.log('Conexão com Banco de Dados Aberta.');
});

var app = express();

app.locals.loggedInUser = {
	isAdmin : false
};
app.locals.user = {};


passport.use(new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, username, password, done) {
		/*
		// nota pra adicionar no banco. 
		var Admin = new Users({
			username: 'admin',
			password: '11e855d9b6058a14fc28a518f002e128',
			name: 'Admin',
			email: 'admin@localhost',
			isAdmin: true
		});
		Admin.save(); 
		*/
		Users.findOne({ 'username': username }).exec(function (err, user) {
			if (err) {
				return done(err);
			}
			if (!user) {
				return done(null, false);
				
			} else { // usuário encontrado. Verificar se a senha está correta.
				var hash = crypto.createHash('md5').update(password).digest("hex");
				if (hash !== user.password) {
					return done(null, false);
				}
				app.locals.user = user;
				return done(null, user, {});
			}
		});
	}
));

passport.serializeUser(function (user, cb) {
	cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
	Users.findById(id, function (err, user) {
		if (err) { return cb(err); }
		if(user === null){
			cb(null, user);	
		
		} else {
			app.locals.user = user;
			cb(null, user);
		}
	});
});

// preparando views engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(function(req, res, next){
	res.setHeader('Upgrade-Insecure-Requests', "1");
	res.setHeader('Access-Control-Allow-Origin', "*");
	res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	next();
});

app.use(session({
	store: redisStore,
	secret: 'fpojnweponw2342roj2nfpo2n',
	resave: false,
	saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/api', api);

app.use('/404', function(req, res){
	res.status(404).render('404');
});

app.use(function(req, res, next){
    res.redirect('/404');
});


module.exports = app;
