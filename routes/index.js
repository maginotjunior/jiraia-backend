/*eslint no-new-require: "off"*/
var express = require('express');
var router = express.Router();
var passport = require('passport');

var mongoose = new require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

require('../models/Sessions');

/* Página de Login é a raiz */
router.all('/', function (req, res) {
	/*
		se estiver logado redireciona para a home
	*/
	if (req.isAuthenticated()) {
		return res.redirect('/home');
	}
	
	var error = '0';
	if (typeof req.query.loginError != 'undefined')
		error = req.query.loginError;
		
		
	
		
    res.render('index', { title: 'Jiraia Back End', loginError : error });
});

// O(N^2) ;~
router.all('/home', authCheck, function(req, res){
	
	var sessionsDb = mongoose.model('Sessions');
	sessionsDb.find({ email: { $exists: true}}).lean().exec(function(err, docs){
		if(err){
			console.error(err);
			return res.status(500).end();
		}
		console.log('docs', docs);
		var docsSorted = [];
		for(var i=0; i<docs.length; i++){
			for(var k=0; k<docs[i].userSession.length; k++){
				docs[i].userSession[k].email = docs[i].email;	
			}
			docsSorted = docsSorted.concat(docs[i].userSession);
		}
		console.log(docsSorted);
		res.render('dashboard', { title : 'Início', sessions: docsSorted});
	});
	
});

router.post('/login', function (req, res) {
	/*
		realiza o login do usuário
	*/
	passport.authenticate('local', {
		failureRedirect: '/?loginError=1',
	})(req, res, function () {
		
		return res.redirect('/home');
	})

});

router.get('/session/:email', authCheck, function(req, res){
	var email = req.params.email;
	if(typeof email === 'undefined') return res.redirect('/home');
	
	var sessionsDb = mongoose.model('Sessions');
	sessionsDb.find({ email: email })
	.lean().exec(function(err, docs){
		if(err){
			console.error(err);
			return res.status(500).end();
		}
		console.log(docs);
		res.render('session', { session: docs});
	});
});



router.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});

function authCheck(req, res, next){
	if (req.isAuthenticated()) {
		next();
	} else {
		return res.redirect('/?loginError=2');
	}
}


module.exports = router;