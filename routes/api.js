var express = require('express');
var router = express.Router();

var mongoose = new require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var redis = require("redis");

require('../models/Sessions');

var redis_url, rPort, rHost;
if (process.env.REDIS_URL) {
	redis_url = process.env.REDIS_URL;
} else {
	rPort = 6379;
	rHost = '127.0.0.1';
	redis_url = "redis://" + rHost + ":" + rPort;
}

var redisClient = redis.createClient(redis_url);

// Api router
router.get('/', function (req, res) {
    res.send("you're using it wrong!");
});

router.get('/token', function(req, res){
	console.log('flag 1');
	try {
		require('crypto').randomBytes(48, function(err, buffer) {
			if(err){
				console.log('Error inside randomBytes');
				return res.status(500).end();
			}
			var token = buffer.toString('hex');
			console.log(token);
			
			var Sessions = mongoose.model("Sessions");
			var doc = {
				token: token,
				userSession: {}
			};
			var newSession = new Sessions(doc);
			newSession.save(function(err){
				if(err) {
					console.log('Mongo session not saved');
					return res.status(500).end();
				}
				redisClient.set(token, 1);
				
				return res.status(200).send(token);	
			});
		});
	} catch(e){
		console.log('Exception: ', e);
		return res.status(500).end();
	}
});

router.get('/token/:token', function(req, res){
	console.log('flag 2');
	try {
		var token = req.params.token;
		console.log('token', token);
		if(!token) return res.status(403).end();
		redisClient.get(token, function(err, value){
			if(err){
				console.log(err);
				return res.status(500).end();
			}
			if(value !== null){
				console.log('value found: ', value);
				return res.status(204).end();
			} else {
				// slow search (using persistent mongo storage)
				var Sessions = mongoose.model('Sessions');
				Sessions.findOne({ 'token' : token }).exec(function(err, doc){
					var status = 404;
					if(err) status = 500;
					if(doc) {
						status = 204;
						// let's put it on storage then
						redisClient.set(token, 1);
					}
					return res.status(status).end();
				});
			}
		});
	} catch(e) {
		console.log("Exception", e);
		return res.status(500).end();
	}
});

function formatDataObj(data){
	var obj = {};
	if(typeof data != 'undefined'){
		for(var i=0; i<data.length; i++){
			obj[data[i].model] = data[i].value;
		}
	}
	return obj;
}

router.post('/session/save/:token', function(req, res){
	var token = req.params.token;
	if(!token) return res.status(403).end();
	console.log('saving session', token);
	console.log(req.body);
	
	var $update = { 
		$push : { 
			userSession: req.body.userSession
		}
	};
	var obj = formatDataObj(req.body.data);
	if(obj.hasOwnProperty("email")){
		$update.$push.data = obj;
		$update.email = obj.email
	}
	console.log("$update", $update);
	var Sessions = mongoose.model('Sessions');
	Sessions.update({ 'token': token}, $update, function(err, numUpdates){
		if(err){
			return res.status(500).end();
		}
		if(numUpdates === 0) return res.status(404).end();
		try {
			redisClient.set(token, 1);
		} catch(e){
			console.log('Error salvando sessão no redis', e);
		}
		return res.status(204);
	});
	return res.status(204).end();
});

module.exports = router;
