var config = {
	redisServerAddress: 'localhost',
	dbServerAddress: 'localhost',
	dbPath: '/maginot',
	dbProtocol: 'mongodb://'
};

module.exports = config;