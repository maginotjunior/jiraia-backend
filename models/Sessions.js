var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Sessions = new Schema({
	token: {
	    type: String,
	    required: true,
	    dropDups: true,
	    index: true
	},
	email: {
		type: String,
		index: true,
	},
	data: Object,
	userSession: {
	    type: Object,
	    default: []
	},
	created: {
	    type: Date,
	    default: new Date()
	}
}, {
	collection: 'Sessions'
});

mongoose.model("Sessions", Sessions);
