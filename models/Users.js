﻿var mongoose = require('mongoose');
var  Schema = mongoose.Schema;

var Users = new Schema({
	username: { type: String, index: true },
	password: String,
	name: String,
	email: String,
	isAdmin: Boolean
}, {
	collection: 'Users'
});

mongoose.model("Users", Users);
